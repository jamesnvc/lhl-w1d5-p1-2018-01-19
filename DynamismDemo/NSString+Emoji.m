//
//  NSString+Emoji.m
//  DynamismDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "NSString+Emoji.h"

@implementation NSString (Emoji)

- (NSString *)emojify
{
    return [self stringByReplacingOccurrencesOfString:@":D" withString:@"😀"];
}

@end
