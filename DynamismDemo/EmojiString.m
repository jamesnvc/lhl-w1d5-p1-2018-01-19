//
//  EmojiString.m
//  DynamismDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "EmojiString.h"

@implementation EmojiString

- (instancetype)initWithString:(NSString *)string
{
    self = [super initWithString:string];
    return self;
}

- (EmojiString*)emojify {
    NSString* s = [self stringByReplacingOccurrencesOfString:@":D" withString:@"😀"];
    return [[EmojiString alloc] initWithString:s];
}

@end
