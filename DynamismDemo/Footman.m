//
//  Footman.m
//  DynamismDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Footman.h"

@interface Footman ()

@property (nonatomic,assign) NSInteger fightTimes;

@end

@implementation Footman

- (instancetype)init
{
    self = [super init];
    if (self) {
        _health = 10;
        _fightTimes = 0;
    }
    return self;
}

- (void)fight
{
    _health -= arc4random_uniform(5);
    self.fightTimes += 1;
    NSLog(@"Finished fight %ld", self.fightTimes);
}

@end
