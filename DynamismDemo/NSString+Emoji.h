//
//  NSString+Emoji.h
//  DynamismDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Emoji)

- (NSString*)emojify;

@end
