//
//  main.m
//  DynamismDemo
//
//  Created by James Cash on 19-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "EmojiString.h"
#import "NSString+Emoji.h"
#import "Footman.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *a1 = @[@"a", @"b", @"c"];
        NSArray *a2 = [NSMutableArray arrayWithArray:a1];
        NSArray<NSArray*>* arrays = @[a1, a2];
        for (NSArray* a in arrays) {
//            [a addObject:@"d"];
            SEL addObj = @selector(addObject:);
            if ([a respondsToSelector:addObj]) {
                [a performSelector:addObj withObject:@"d"];
            }
        }
        NSLog(@"Array 1: %@", a1);
        NSLog(@"Array 2: %@, %@", a2, arrays[1]);
//        NSDictionary<NSString*, NSArray*>* dict;

//        NSLog(@"%@", arrays[0].count);

//        EmojiString *es = [[EmojiString alloc] initWithString:@"Hi :D"];
//        NSLog(@"%@", [es emojify]);

        NSString *s = @"Hi :D";
        NSLog(@"Before: '%@' After '%@'", s, s.emojify);
        NSLog(@"String length: %ld", s.length);


        Footman *f = [[Footman alloc] init];
        NSLog(@"Health %ld", f.health);
        [f fight];
        NSLog(@"Health %ld", f.health);
        [f fight];
        NSLog(@"Health %ld", f.health);
        [f fight];
        NSLog(@"Health %ld", f.health);

//        [f performSelector:@selector(setHealth:) withObject:@10];
        NSLog(@"Fought %ld times", [f performSelector:@selector(fightTimes)]);

        [f performSelector:@selector(setFightTimes:) withObject:@100];
        [f fight];
        NSLog(@"Health %ld", f.health);


    }
    return 0;
}
